#!/usr/bin/env sh

set -ex

rm -rf ./acceptance/new-test-adrs
./acceptance/create-test.exp

EXPECTED_CONTENT="$(cat acceptance/expected-doc.md | grep -v Date: )"
ACTUAL_CONTENT="$(cat ./acceptance/new-test-adrs/0000-title.md | grep -v Date: )"

rm -rf ./acceptance/new-test-adrs

if [ "$EXPECTED_CONTENT" != "$ACTUAL_CONTENT" ]
then
    echo Got unexpected result
    exit 1
fi

./acceptance/link-test.exp

STATUS_LINE="$(cat ./acceptance/test-adrs/0001-title.md | head -n 3 | tail -n 1 )"

if [ "$STATUS_LINE" != "* Status: accepted, supersedes [ADR-0000](0000-title.md)" ]
then
    echo Got unexpected result
    exit 1
fi
