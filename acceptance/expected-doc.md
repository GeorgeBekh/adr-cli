# Some title

* Status: rejected
* Deciders: Some decider1, Some decider2
* Date: Tue Mar 19 16:16:39 MSK 2019

## Context and Problem Statement

Some context

## Decision Drivers

* Some decision driver1
* Some decision driver2


## Considered Options

* Some considered option1
* Some considered option2


## Decision Outcome

Some decision outcome

### Positive Consequences

* Some positive consequence1
* Some positive consequence2

### Negative Consequences

* Some negative consequence1
* Some negative consequence2

