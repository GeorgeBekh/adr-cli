build:
	@go build

test:
	@./acceptance/test.sh

clean:
	@rm -rf ./adr-cli ./doc
