package main

import (
	"strings"

	"github.com/leonelquinteros/gotext"
)

func markdownRender(rendered *adr, po *gotext.Po) string {
	response := ""

	response += "# " + rendered.Title + "\n"
	response += "\n"
	if rendered.Status != "" {
		response += getMarkdownStatus(po) + rendered.Status + "\n"
	}
	if len(rendered.Deciders) != 0 {
		response += "* " + po.Get("Deciders: ") + strings.Join(rendered.Deciders, ", ") + "\n"
	}

	if rendered.Date != "" {
		response += "* " + po.Get("Date: ") + rendered.Date + "\n"
	}

	response += "\n"
	response += "## " + po.Get("Context and Problem Statement") + "\n"
	response += "\n"
	response += rendered.ContextAndProblem
	response += "\n"

	if len(rendered.DecisionDrivers) != 0 {
		response += "\n"
		response += "## " + po.Get("Decision Drivers") + "\n"
		response += "\n"
		response += "* " + strings.Join(rendered.DecisionDrivers, "\n* ") + "\n"
		response += "\n"
	}

	if len(rendered.ConsideredOptions) != 0 {
		response += "\n"
		response += "## " + po.Get("Considered Options") + "\n"
		response += "\n"
		response += "* " + strings.Join(rendered.ConsideredOptions, "\n* ") + "\n"
		response += "\n"
	}

	response += "\n"
	response += "## " + po.Get("Decision Outcome") + "\n"
	response += "\n"
	response += rendered.DecisionOutcome + "\n"
	response += "\n"

	if len(rendered.PositiveConsequences) != 0 {
		response += "### " + po.Get("Positive Consequences") + "\n"
		response += "\n"
		response += "* " + strings.Join(rendered.PositiveConsequences, "\n* ") + "\n"
		response += "\n"
	}

	if len(rendered.NegativeConsequences) != 0 {
		response += "### " + po.Get("Negative Consequences") + "\n"
		response += "\n"
		response += "* " + strings.Join(rendered.NegativeConsequences, "\n* ") + "\n"
		response += "\n"
	}

	return response
}

func getMarkdownStatus(po *gotext.Po) string {
	return "* " + po.Get("Status: ")
}
