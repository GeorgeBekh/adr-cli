package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/abiosoft/ishell"
	"github.com/leonelquinteros/gotext"
)

var statuses = []string{
	"Skip",
	"proposed",
	"rejected",
	"accepted",
	"deprecated",
}

func newCommand(po *gotext.Po) *ishell.Cmd {
	return &ishell.Cmd{
		Name: "new",
		Help: po.Get("create new adr"),
		Func: func(c *ishell.Context) {
			c.ShowPrompt(false)
			defer c.ShowPrompt(true)

			newAdr := adr{}

			// Title

			var err error
			newAdr.Title, err = prompt("Short title of solved problem and solution: ", c, po)
			if err != nil {
				return
			}

			// Status

			translatedStatuses := translateMessages(statuses, po)
			statusIndex := c.MultiChoice(translatedStatuses, po.Get("Choose status"))
			status := ""
			if statusIndex == -1 {
				c.Println()
				return
			}

			if statusIndex > 0 {
				status = translatedStatuses[statusIndex]
			}

			newAdr.Status = status

			// Deciders

			newAdr.Deciders, err = enterMany(
				"Add deciders?",
				"Decider: ",
				c,
				po,
			)
			if err != nil {
				return
			}

			// Date

			newAdr.Date = time.Now().Format("02-01-2006")

			// Context and problem

			newAdr.ContextAndProblem, err = prompt("Describe the context and problem statement: ", c, po)
			if err != nil {
				return
			}

			// Decision drivers

			newAdr.DecisionDrivers, err = enterMany(
				"Add decision drivers?",
				"Decision driver: ",
				c,
				po,
			)
			if err != nil {
				return
			}

			// Considered options

			newAdr.ConsideredOptions, err = enterMany(
				"Add considered options?",
				"Considered option: ",
				c,
				po,
			)
			if err != nil {
				return
			}

			// Decision outcome

			newAdr.DecisionOutcome, err = prompt("Decision outcome: ", c, po)
			if err != nil {
				return
			}

			// Positive consequences

			newAdr.PositiveConsequences, err = enterMany(
				"Add positive consequences?",
				"Positive consequence: ",
				c,
				po,
			)
			if err != nil {
				return
			}

			// Negative consequences

			newAdr.NegativeConsequences, err = enterMany(
				"Add negative consequences?",
				"Negative consequence: ",
				c,
				po,
			)
			if err != nil {
				return
			}

			createDirectory()
			files := listFiles()
			filename := fmt.Sprintf("%s/%s", directory, generateFilename(files))

			save(filename, markdownRender(&newAdr, po))
		},
	}
}

func generateFilename(filenames []string) string {
	lastDocFilename := ""
	sort.Strings(filenames)

	for _, filename := range filenames {
		if isAdr(filename) {
			lastDocFilename = filename
		}
	}

	if lastDocFilename == "" {
		return "0000-title.md"
	} else {
		nameParts := strings.Split(lastDocFilename, "-")
		if len(nameParts) == 0 || nameParts[0] == "" {
			log.Fatalf("Invalid filename: %s", lastDocFilename)
		}

		lastDocNumber, err := strconv.Atoi(nameParts[0])
		if err != nil {
			log.Fatal(err)
		}

		return fmt.Sprintf("%04d-title.md", lastDocNumber+1)
	}
}

func createDirectory() {
	os.MkdirAll(directory, 0777)
}

func save(filename, content string) {
	err := ioutil.WriteFile(filename, []byte(content), 0644)

	if err != nil {
		log.Fatal(err)
	}
}

func enterMany(promptMany, promptOne string, c *ishell.Context, po *gotext.Po) ([]string, error) {
	many := []string{}
	enter := c.MultiChoice(translateMessages([]string{"no", "yes"}, po), po.Get(promptMany))
	if enter == -1 {
		c.Println()
		return nil, fmt.Errorf("Keyboard interrupt")
	}

	if enter == 1 {
		value, err := prompt(promptOne, c, po)
		if err != nil {
			return nil, err
		}

		for value != "" {
			many = append(many, value)
			value, err = prompt(promptOne, c, po)
			if err != nil {
				return nil, err
			}
		}
	}

	return many, nil
}
