package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/abiosoft/ishell"
	"github.com/leonelquinteros/gotext"
	"github.com/rakyll/statik/fs"

	_ "gitlab.com/GeorgeBekh/adr-cli/statik"
)

var directory string

func main() {
	shell := ishell.New()

	po := getTranslator(getLanguage())
	directory = getDirectory()

	shell.AddCmd(newCommand(po))
	shell.AddCmd(linkCommand(po))

	if len(os.Args) > 1 {
		shell.Process(os.Args[1:]...)
	} else {
		shell.Run()
	}
}

func prompt(question string, c *ishell.Context, po *gotext.Po) (string, error) {
	c.Print(po.Get(question))
	response, err := c.ReadLineErr()
	if err != nil {
		return "", err
	}

	return response, nil
}

func isAdr(filename string) bool {
	if len(filename) < 5 {
		return false
	}

	return filename[4] == '-' && strings.HasSuffix(filename, ".md")
}

func listFiles() []string {
	files, err := ioutil.ReadDir(directory)

	filenames := make([]string, len(files))

	if err != nil {
		log.Fatal(err)
	}

	for key, file := range files {
		filenames[key] = file.Name()
	}

	return filenames
}

func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

func getTranslator(lang string) *gotext.Po {
	if lang == "en" {
		return &gotext.Po{}
	}

	statikFs, err := fs.New()
	if err != nil {
		log.Fatal(err)
	}

	fileName := "/messages-" + lang + ".po"
	file, err := statikFs.Open(fileName)
	if err != nil {
		fmt.Printf("failed reading %s: %s\n", fileName, err)
		return &gotext.Po{}
	}

	content, err := ioutil.ReadAll(file)

	if err != nil {
		fmt.Printf("failed reading %s: %s\n", fileName, err)
		return &gotext.Po{}
	}

	po := &gotext.Po{}
	po.Parse(content)

	return po
}

func getLanguage() string {
	langEnv := os.Getenv("LANG")
	langParts := strings.Split(langEnv, "_")
	if len(langParts) == 0 || langParts[0] == "" {
		return "en"
	}

	return langParts[0]
}

func getDirectory() string {
	dirEnv := os.Getenv("DIR")

	if dirEnv == "" {
		return "./doc/adr/"
	} else {
		return dirEnv
	}
}

func translateMessages(messages []string, po *gotext.Po) []string {
	translated := []string{}

	for _, message := range messages {
		translated = append(translated, po.Get(message))
	}

	return translated
}

type adr struct {
	Title                string
	Status               string
	Deciders             []string
	Date                 string
	ContextAndProblem    string
	DecisionDrivers      []string
	ConsideredOptions    []string
	DecisionOutcome      string
	PositiveConsequences []string
	NegativeConsequences []string
	//TODO: ProsAndConsOfOptions
	//TODO: Links
}
