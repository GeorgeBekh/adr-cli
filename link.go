package main

import (
	"io/ioutil"
	"log"
	"strings"

	"github.com/abiosoft/ishell"
	"github.com/leonelquinteros/gotext"
)

var linkTypes = [][]string{
	{"supersedes", "superseded by"},
	{"amends", "amended by"},
}

func linkCommand(po *gotext.Po) *ishell.Cmd {
	return &ishell.Cmd{
		Name: "link",
		Help: po.Get("link two adrs"),
		Func: func(c *ishell.Context) {
			c.ShowPrompt(false)
			defer c.ShowPrompt(true)

			adrs := getAdrs()

			linkFrom := c.MultiChoice(adrs, po.Get("Choose file to link from"))
			if linkFrom == -1 {
				return
			}

			adrsToLinkTo := withoutElement(adrs, linkFrom)

			linkTo := c.MultiChoice(adrsToLinkTo, po.Get("Choose file to link to"))
			if linkTo == -1 {
				return
			}

			linkTypeIndex := c.MultiChoice(getLinkTypesView(po, adrs[linkFrom], adrsToLinkTo[linkTo]), po.Get("Choose link type"))
			if linkTypeIndex == -1 {
				return
			}

			createLink(directory+"/"+adrs[linkFrom], linkTypes[linkTypeIndex][0], adrsToLinkTo[linkTo], po)
			createLink(directory+"/"+adrsToLinkTo[linkTo], linkTypes[linkTypeIndex][1], adrs[linkFrom], po)
			c.Println(po.Get("Adrs linked successfully"))
		},
	}
}

func withoutElement(slice []string, i int) []string {
	newSlice := make([]string, len(slice))
	copy(newSlice, slice)
	newSlice = append(newSlice[:i], newSlice[i+1:]...)

	return newSlice
}
func getLinkTypesView(po *gotext.Po, linkFrom, linkTo string) []string {
	linkTypesView := make([]string, len(linkTypes))
	for i, linkType := range linkTypes {
		linkTypesView[i] = linkFrom + " " + po.Get(linkType[0]) + " " + linkTo
	}

	return linkTypesView
}

func createLink(filename string, status, adr string, po *gotext.Po) {
	input, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalln(err)
	}

	lines := strings.Split(string(input), "\n")

	statusLine := -1
	for i, line := range lines {
		if strings.Contains(line, getMarkdownStatus(po)) {
			statusLine = i
			break
		}
	}

	addedStatus := po.Get(status) + " [ADR-" + adr[:4] + "]" + "(" + adr + ")"

	if statusLine != -1 {
		if !strings.Contains(lines[statusLine], addedStatus) {
			lines[statusLine] = lines[statusLine] + ", " + addedStatus
		}
	} else {
		lines = insert(getMarkdownStatus(po)+addedStatus, 2, lines)
	}

	output := strings.Join(lines, "\n")
	err = ioutil.WriteFile(filename, []byte(output), 0644)
	if err != nil {
		log.Fatalln(err)
	}
}

func insert(element string, i int, s []string) []string {
	s = append(s, "")
	copy(s[i+1:], s[i:])
	s[i] = element

	return s
}

func getAdrs() []string {
	adrs := []string{}

	files := listFiles()
	for _, filename := range files {
		if isAdr(filename) {
			adrs = append(adrs, filename)
		}
	}

	return adrs
}
