package main

import "testing"

func TestGenerateFilename(t *testing.T) {
	filename := generateFilename([]string{
		"0003-some-other-doc.md",
		"0001-some-doc.md",
		"0002-some-other-doc.md",
		"someRandomFile.png",
		"z.png",
	})

	if filename != "0004-title.md" {
		t.Errorf("generateFilename(...) = %s; want 0004-title.md", filename)
	}
}
