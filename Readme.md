# Adr cli

This interactive cli tool is supposed to make managing and creating [madrs](https://github.com/adr/madr) easier.

## Features

* Interactive madr creation
* Linking
* I18n

## Usage

```sh
# Create new adr: ./doc/adr/0000-title.md
$ adr-cli new
Short title of solved problem and solution: Use framework X everywhere
Choose status
   Skip
   proposed
   rejected
 ❯ accepted
   deprecated
Add deciders?
   no
 ❯ yes
Decider: Joe
Decider: Lynda
Decider:
...
```

### Configure with enviromental variables

```
# Create new adr in russian: ./custom-dir/0000-title.md
$ DIR=./custom-dir LANG=ru adr-cli new
```


### Linking

```
$ adr-cli link
Choose file to link from
   0001-some-adr.md
 > 0002-some-other-adr.md

Choose file to link to
 > 0001-some-adr.md

Choose link type
 > 0002-some-other-adr.md supersedes 0001-some-adr.md
   0002-some-other-adr.md amends 0001-some-adr.md
```

**0001-some-adr.md**
```
# Some title 1

* Status: superseded by [ADR-0002](0002-some-other-adr.md)
```

**0002-some-other-adr.md**
```
# Some other title 2

* Status: accepted, supersedes [ADR-0001](0001-some-adr.md)
```


## Currently supported languages

* English
* Russian

## Roadmap

 - [x] Saving to file
 - [x] I18n
 - [x] Linking
 - [x] Acceptance tests
 - [ ] Unit tests
